#!/usr/bin/python
# -- coding: utf-8 --
#--------------------------------------
#  Wifi Radio script
#  
#
# Author : Lukas Barak
# Date   : 21/12/2017#
#
# project source and wiki: https://bitbucket.org/chashitsu/radio/overview
#--------------------------------------
import smbus
#import RPi.GPIO as GPIO #RPi version
import time
import os
import sys
import vlc # audio player

import lcd_cesky # lcd library wraper to support czech characters
import codecs # read file in utf-8 coding

if not os.getegid() == 0:
    sys.exit('Script must be run as root')


from pyA20.gpio import gpio
from pyA20.gpio import connector
from pyA20.gpio import port


# buttons definition
# ==================
button_next = port.PA1  #PIN 11 (GPIO0 on RPi)
button_prev = port.PA0  #PIN 13 (GPIO2 on RPi)

button_fav1 = port.PA3  #PIN 15 
button_fav2 = port.PA6  #PIN 7
button_fav3 = port.PA13 #PIN 8
button_fav4 = port.PA14	#PIN 10

button_select = port.PC0 #PIN 19
button_menu = port.PC1 # PIN 21




def showIp():
      print('getting ip')
      os.system("ip -4 a | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' > /root/radio/current_ip")
      f = open('/root/radio/current_ip','r')
      lines = str.splitlines(f.read())
      if len(lines) == 2:
         lcd_cesky.lcdPrint2('Wire:'+lines[0],'WiFi:' + lines[1])
      if len(lines) == 1:
         lcd_cesky.lcdPrint("Wire:" + lines[0])
      time.sleep(0.2) # sleep to debounc button

# radioClass
class RadioStation(object):
  def __init__(self, index, parts):
    self.index = index
    self.name = parts[0]
    self.description = parts[1]
    self.shortcut = parts[2]
    self.logo = parts[3]
    self.isOnAir = parts[4]
    self.style = parts[5]
    self.region = parts[6]
    self.url = parts[7]
    self.bitrate = parts[8]
    self.podcast = parts[9]
    self.isPlayCz = parts[10]

def getStations():
  lines = codecs.open('/root/radio/radios2.csv', encoding='utf-8')
#0 Title~ 1 Description~ 2 Shortcut~ 3 logo~ 4 OnAir~ 5 Style~ 6 Region~ 7 Stream~ 8 Bitrate~9 Podcast~ 10 IsPlayCz
  i = -1
  raList = []
  for item in lines:
    
    if i == -1 :
      i=0
      continue
    itemParts = item.split('~')
    if len(itemParts) >= 11 :	
      ra = RadioStation(i,itemParts)
      raList.append(ra)
      i+=1
  return raList 


def getFavourites( num, radios ):
  lines = open('/root/radio/radiosFav.csv','r')
  favList = []
  for item in lines:
    if len(item.strip())>1:
      #print(item)
      favList.append(item)
  
  result = radios[0]  
  print(len(favList))
  if len(favList) == 4 :
    fav = favList[num] 
  
  print('fav: '+fav)   
  for ra in radios :
    #print(ra.shortcut)
    if ra.shortcut.strip() == fav.strip() :
      result = ra
      #print('-----aaa: '+ra.shortcut)

  #print('result'+result.shortcut)  
  return result


def getOnAir(r):
  if (r.onAirUrl != '') and (r.onAirUrl != 'false'):
    a = 1

# MAIN
# IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
# IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
def main():

  lcd_cesky.lcdPrint("START RadioScript  <")
  time.sleep(1)
  
  # Init button
  # ==============
  gpio.init()  
  gpio.setcfg(button_next, gpio.INPUT)
  gpio.pullup(button_next, gpio.PULLUP)

  gpio.setcfg(button_prev, gpio.INPUT)
  gpio.pullup(button_prev, gpio.PULLUP)  

  gpio.setcfg(button_select, gpio.INPUT)
  gpio.pullup(button_select, gpio.PULLUP)

  gpio.setcfg(button_menu, gpio.INPUT)
  gpio.pullup(button_menu, gpio.PULLUP)

  gpio.setcfg(button_fav1, gpio.INPUT)
  gpio.pullup(button_fav1, gpio.PULLUP)

  gpio.setcfg(button_fav2, gpio.INPUT)
  gpio.pullup(button_fav2, gpio.PULLUP)

  gpio.setcfg(button_fav3, gpio.INPUT)
  gpio.pullup(button_fav3, gpio.PULLUP)

  gpio.setcfg(button_fav4, gpio.INPUT)
  gpio.pullup(button_fav4, gpio.PULLUP)

  raList = getStations()
  
  raCurrent = 0
  lcd_cesky.lcdPrint2(raList[raCurrent].name, raList[raCurrent].description[:60]) 
  p = vlc.MediaPlayer(raList[raCurrent].url)
  p.play()
  pplay = True

  ms = time.time()*1000.0
  light = True

  while True:
    time.sleep(0.005) # to prevent CPU heating ...

    # Turn of the lights after 10s
    if light and ((time.time()*1000.0)-ms) > 10000 :
      light = False
      lcd_cesky.lightOff()


    # NEXT BUTTON
    if gpio.input(button_prev) == False:
#      print("next")
      if not light :
        lcd_cesky.lightOn()
        light = True
        ms = time.time()*1000.0
      raCurrent+=1
      if raCurrent >= len(raList):
        raCurrent=0
      p.stop()
      p = vlc.MediaPlayer(raList[raCurrent].url)
      p.play()
      pplay = True
      lcd_cesky.lcdPrint2(raList[raCurrent].name, raList[raCurrent].description[:60])
      time.sleep(0.2) # sleep to debounc button

    # PREVIOUS BUTTON
    if gpio.input(button_next) == False:
#      print("previous")
      if not light :
        lcd_cesky.lightOn()
        light = True
        ms = time.time()*1000.0 
      raCurrent-=1
      if raCurrent < 0:
        raCurrent=len(raList)-1
      p.stop()
      p = vlc.MediaPlayer(raList[raCurrent].url)
      p.play()
      pplay = True
      lcd_cesky.lcdPrint2(raList[raCurrent].name, raList[raCurrent].description[:60])
      time.sleep(0.2) # sleep to debounc button

    # STOP/PLAY / SELECT BUTTON
    if gpio.input(button_select) == False:
      if not light :
        lcd_cesky.lightOn()
        light = True
        ms = time.time()*1000.0  
      showIp()
      if pplay:
        p.stop()
        pplay=False
      else:
        p.play()
	pplay=True
      time.sleep(0.2) # debouncing

    # FAVOURITE 4
    if gpio.input(button_fav4) == False:
      #print("fav4!!!")
      if not light :
        lcd_cesky.lightOn()
        light = True
        ms = time.time()*1000.0  
      toPlay=getFavourites(3,raList)
      p.stop();
      p = vlc.MediaPlayer(toPlay.url)
      p.play()
      pplay = True
      lcd_cesky.lcdPrint2(toPlay.name, toPlay.description[:60])
      time.sleep(0.2) # sleep to debounc button

    # FAVOURITE 3
    if gpio.input(button_fav3) == False:
      #print("fav3!!!")
      if not light :
        lcd_cesky.lightOn()
        light = True
        ms = time.time()*1000.0  
      toPlay=getFavourites(2,raList)
      p.stop();
      p = vlc.MediaPlayer(toPlay.url)
      p.play()
      pplay = True
      lcd_cesky.lcdPrint2(toPlay.name, toPlay.description[:60])
      time.sleep(0.2) # sleep to debounc button

    # FAVOURITE 2
    if gpio.input(button_fav2) == False:
      #print("fav2!!!")
      if not light :
        lcd_cesky.lightOn()
        light = True
        ms = time.time()*1000.0  
      toPlay=getFavourites(1,raList)
      p.stop();
      p = vlc.MediaPlayer(toPlay.url)
      p.play()
      pplay = True
      lcd_cesky.lcdPrint2(toPlay.name, toPlay.description[:60])
      time.sleep(0.2) # sleep to debounc button


    # MENU
    if gpio.input(button_menu) == False:
      showIp()
      time.sleep(0.2)


# START MAIN
main()
#if __name__ == '__main__':
#    try:
#        main()
#    except Exception as e:
#        logging.debug(e)
#        raise
