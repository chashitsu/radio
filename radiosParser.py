#!/usr/bin/python
# -- coding: utf-8 --
import sys
import csv
import requests
from lxml import etree
from StringIO import StringIO


# open the source xml file!
with open('./playCzAllRadios.xml', 'r') as content_file:
    xml = content_file.read()


with open('output.csv', 'w') as f:
    writer = csv.writer(f, delimiter ="~", quoting=csv.QUOTE_MINIMAL)
    writer.writerow(('Title', 'Description', 'Shortcut', 'logo', 'OnAir', 'Style', 'Region', 'Stream', 'Bitrate', 'Podcast' , 'IsPlayCz'))
    
    tree = etree.parse(StringIO(xml))
    context = etree.iterparse(StringIO(xml))

    root = tree.find("data")
    insideData = False
    i = 0

    for i in range(0,len(root)) :
#	print root[i].tag + str(i)
	for b in root[i] :	
		text = b.text.encode('utf-8').strip() 
		if b.tag == 'title' :
			title = text
		if b.tag == 'description' :
			description = text
		if b.tag == 'shortcut' :
			shortcut = text
		if b.tag == 'logo' :
			logo = text
		if b.tag == 'onair' :
			onair = text
		if b.tag == 'style' :
		# now, there should be an list of styles!
			styles = ''
			for c in b :
				styles += c.text.strip() + '|'
		if b.tag == 'region' :
			regions = ''
                        for c in b :
                                regions += c.text.strip() + '|'
	styles = styles.encode('utf-8').strip()
	regions = regions.encode('utf-8').strip() 

	print "parsing: " + title
	streamUrl = 'http://api.play.cz/xml/getStream/' + shortcut + '/mp3/256'
	streamInfo = requests.get(streamUrl)
	streamXmlTree = etree.parse(StringIO(streamInfo.content))
	item = streamXmlTree.find("data/stream/pubpoint")
	stream = item.text
	bitrate =  streamXmlTree.find("data/stream/stream_bitrate").text
	
	print "OK"

	writer.writerow((title, description, shortcut, logo, onair, styles, regions, stream, bitrate, 'Podcast', '1'))	
		
	
