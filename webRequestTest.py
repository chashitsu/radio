#!/usr/bin/python
# -- coding: utf-8 --
import sys
import csv
from lxml import etree
from StringIO import StringIO

import requests

shortcut = 'radio1'

streamUrl = 'http://api.play.cz/xml/getStream/' + shortcut + '/mp3/256'
streamInfo = requests.get(streamUrl)
streamXmlTree = etree.parse(StringIO(streamInfo.content))
item = streamXmlTree.find("data/stream/pubpoint")
print item.tag
print item.text


