#!/usr/bin/python
# -- coding: utf-8 --
# requires RPi_I2C_driver.py
import RPi_I2C_driver
import os
import requests
import json
from time import *


mylcd = RPi_I2C_driver.lcd()

mylcd.lcd_clear()


#response = requests.get('http://onair.play.cz/json/cro2.json')
#data = json.loads(response.content)


#for c in data[song]:

#mylcd.lcd_display_string_pos(data['song'],1,1) # row 1, column 1


# all avalible special characters
fontData = [
        [ 0x0a, 0x04, 0x1f, 0x02, 0x04, 0x08, 0x1f, 0x00 ], # ž 0
        [ 0x05, 0x02, 0x08, 0x1c, 0x08, 0x08, 0x09, 0x06 ], # ť 1
        [ 0x0a, 0x04, 0x00, 0x0e, 0x10, 0x10, 0x11, 0x0e ], # č 2
        [ 0x0c, 0x12, 0x12, 0x12, 0x11, 0x11, 0x12, 0x00 ], # ß 3
        [ 0x0a, 0x00, 0x0e, 0x01, 0x0f, 0x11, 0x0f, 0x00 ], # ä 4
        [ 0x02, 0x04, 0x00, 0x0e, 0x11, 0x11, 0x11, 0x0e ], # ó 5
        [ 0x0a, 0x04, 0x00, 0x16, 0x19, 0x11, 0x11, 0x11 ], # ň 6
        [ 0x09, 0x00, 0x11, 0x11, 0x11, 0x13, 0x0d, 0x00 ], # ü 7
        [ 0x02, 0x04, 0x00, 0x0e, 0x01, 0x0f, 0x11, 0x0f ], # á 8
        [ 0x0a, 0x04, 0x00, 0x16, 0x19, 0x10, 0x10, 0x10 ], # ř 9
        [ 0x04, 0x0a, 0x04, 0x11, 0x11, 0x11, 0x13, 0x0d ], # ů 10
        [ 0x02, 0x04, 0x11, 0x11, 0x11, 0x13, 0x0d, 0x00 ], # ú 11
        [ 0x02, 0x04, 0x11, 0x11, 0x0f, 0x01, 0x0e, 0x00 ], # ý 12
        [ 0x03, 0x04, 0x00, 0x0c, 0x04, 0x04, 0x04, 0x0e ], # í 13
        [ 0x02, 0x04, 0x00, 0x0e, 0x11, 0x1f, 0x10, 0x0e ], # é 14
        [ 0x0a, 0x04, 0x00, 0x0e, 0x11, 0x1f, 0x10, 0x0e ], # ě 15
        [ 0x0a, 0x04, 0x00, 0x0e, 0x10, 0x0e, 0x01, 0x1e ], # š 16
	[ 0x0a, 0x05, 0x01, 0x0d, 0x13, 0x11, 0x11, 0x0f ], # ď 17
]

# 6 special characters that can be loaded to LCD controller
fontTmp = [
        [ 0x0c, 0x12, 0x12, 0x12, 0x11, 0x11, 0x12, 0x00 ], # ß 0
        [ 0x0a, 0x00, 0x0e, 0x01, 0x0f, 0x11, 0x0f, 0x00 ], # ä 1
        [ 0x02, 0x04, 0x00, 0x0e, 0x11, 0x11, 0x11, 0x0e ], # ó 2
        [ 0x0a, 0x04, 0x00, 0x16, 0x19, 0x11, 0x11, 0x11 ], # ň 3
        [ 0x09, 0x00, 0x11, 0x11, 0x11, 0x13, 0x0d, 0x00 ], # ü 4
        [ 0x02, 0x04, 0x00, 0x0e, 0x01, 0x0f, 0x11, 0x0f ], # á 5
]

# map to those current 6 special chars with indexes
tmpDic = {} # dictionary to store information about prepared custom fonts

# all special chars with there unspecial replacement (needed if there is more than 6 special chars in one message)
# CONSTANT
specCharsReplaceDict = {u'ď':u'd',u'Ď':u'D',u'ž':u'z',u'Ž':u'Z',u'ť':u't',u'Ť':u'T',u'č':u'c',u'Č':u'C',u'ß':u's',u'ä':u'a',u'Ä':u'A',u'ó':u'o',u'Ó':u'O',u'ň':u'n',u'Ň':u'N',u'ü':u'u',u'Ü':u'U',u'á':u'a',u'Á':u'A',u'ř':u'r',u'Ř':u'R',u'ů':u'u',u'Ů':u'U',u'ý':u'y',u'Ý':u'Y',u'í':u'i',u'Í':u'I',u'é':u'e',u'É':u'E',u'ě':u'e',u'Ě':u'E',u'š':u's',u'Š':u'S'}

def prepareCustomChars(msg) :
        i = 0 # index of custom char 0 - 5

	tmpDic = {}
	
#	for f in tmpDic :
#		del f

        # first loop to prepare custom characters
        for c in msg:
                if (c == u"ž" or c == u"Ž") and i < 6 and not (c in tmpDic) :
                        fontTmp[i] = fontData[0]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ť" or c == u"Ť") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[1]
                        tmpDic[c] = i
                        i+=1
                        continue																		
                if (c == u"č" or c == u"Č") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[2]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ß" ) and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[3]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ä" ) and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[4]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ó" or c == u"Ó") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[5]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ň" or c == u"Ň") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[6]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ü" or c == u"Ü") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[7]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"á" or c == u"Á") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[8]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ř" or c == u"Ř") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[9]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ů" or c == u"Ů") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[10]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ú" or c == u"Ú") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[11]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ý" or c == u"Ý") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[12]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"í" or c == u"Í") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[13]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"é" or c == u"É") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[14]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ě" or c == u"Ě") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[15]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"š" or c == u"Š") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[16]
                        tmpDic[c] = i
                        i+=1
                        continue
                if (c == u"ď" or c == u"Ď") and i < 6 and not (c in tmpDic):
                        fontTmp[i] = fontData[17]
                        tmpDic[c] = i
                        i+=1
                        continue
                
	# load chosen chars to LCD
	mylcd.lcd_load_custom_chars(fontTmp)
	return tmpDic

def lightOn() :
  mylcd.backlight(1)

def lightOff() :
  mylcd.backlight(0)

# print to first row, and than the rest
# =====================================
def lcdPrint2(msg1,msg2) :
	
        mylcd.lcd_clear()
        specCh = False
        for c in msg1 + msg2 :
                if c in specCharsReplaceDict :
                        specCh = True
                        break
        tmpDic = {}

        # if msg contain any special character, call the special character preparator
        if specCh :
                tmpDic = prepareCustomChars(msg1[:20] + msg2[:60])


        # cmd to start print from first row
        mylcd.lcd_write(0x80)

        ich = 1 # index of current LCD column
        for c in msg1[:20]:
                if specCh and c in tmpDic : # spec char in loaded chars
                        mylcd.lcd_write_char(tmpDic[c])
                elif specCh and c in specCharsReplaceDict : # spec char not loaded => replace it
                        mylcd.lcd_write_char(ord(specCharsReplaceDict[c]))
                else :
                        mylcd.lcd_write_char(ord(c)) # standart ASCII chars
                ich += 1

	# Print 2nd, 3rd and 4th row
	mylcd.lcd_write(0xc0) # 2nd row
        ich = 1 # index of current LCD column
        for c in msg2[:60]:
                if ich == 21 :
                        mylcd.lcd_write(0x94) # 3rd row
                if ich == 41 :
                        mylcd.lcd_write(0xd4) # 4th row
                if specCh and c in tmpDic : # spec char in loaded chars
                        mylcd.lcd_write_char(tmpDic[c])
                elif specCh and c in specCharsReplaceDict : # spec char not loaded => replace it
                        mylcd.lcd_write_char(ord(specCharsReplaceDict[c]))
                else :
                        mylcd.lcd_write_char(ord(c)) # standart ASCII chars
                ich += 1





# print all 80 chars at once
# ==========================
def lcdPrint(msg) :

	mylcd.lcd_clear()	
	specCh = False
	for c in msg :
		if c in specCharsReplaceDict :
			specCh = True
			break
	tmpDic = {}

	# if msg contain any special character, call the special character preparator
	if specCh :
		tmpDic = prepareCustomChars(msg)

		
        # cmd to start print from first row
        mylcd.lcd_write(0x80)

	ich = 1 # index of current LCD column	
        for c in msg:
		if ich == 21 :
			mylcd.lcd_write(0xc0) # 2nd row
		if ich == 41 :
			mylcd.lcd_write(0x94) # 3rd row
                if ich == 61 :
                        mylcd.lcd_write(0xd4) # 4th row			
                if specCh and c in tmpDic : # spec char in loaded chars
			mylcd.lcd_write_char(tmpDic[c])
                elif specCh and c in specCharsReplaceDict : # spec char not loaded => replace it
			mylcd.lcd_write_char(ord(specCharsReplaceDict[c]))
		else :
			mylcd.lcd_write_char(ord(c)) # standart ASCII chars
		ich += 1


#lcdPrint(u"Žluťoučký kůň pěl ďábelské ódy.")	

#sleep(3)

#mylcd.lcd_clear()
#lcdPrint(u"Řehoř seděl doma a hnil s jablkem v zádech.")		

#sleep(2)

#mylcd.lcd_clear()
#lcdPrint(u"Ať toto je nějaké hódně dlóhé text, s další spóstó znamének. Hadimršťka Obecná")

print("lcd_cesky loaded!")
#quit() 

