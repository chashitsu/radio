# README #

### What is this repository for? ###

* WiFi radio player.
* Based on OrangePi PC Plus (almost same like RPi 3)
* Python (and Vlc player api library)
* Some micro buttons
* YwRobot I2C  char LCD 4x20 characters
* Mostly Czech radio stations (using streaming server play.cz)

### What is working? ###
* play default radio station after startup 
* switch next and previous station
* use one of 4 buttons to switch to favourite station
* switch off LCD backlight after 10s of inactivity
* supports utf8 czceh characters with char LCD (using custom char registers)

### What needs to be done? ###
* make the code simplier and less duplicated (I need to get more familiar with python)
* after reboot check correct wifi connection and than start the stream (High priority issue)
* automatic station list update
* automatic software update (git pull)
* show realtime data about current song (already prepared with play.cz api)
* only 3 of 4 favourite buttons working
* favourite button change after long press
* add second graphic color LCD to show artist image
* add bluetooth output settings
* add support for DVB tunner
* add user interface for wifi connection
* implementsome sort of MENU, include: 
	* adding more stations to favourite
	* backlight timeout
	* update
	


### How do I get set up? ###



### Contribution guidelines ###


### Who do I talk to? ###

* Author: Luks Barak laky.hacker@gmail.com
* Play.cz API https://bitbucket.org/playcz/api.play.cz/wiki/Home
* Play.cz email info@play.cz